import 'package:flutter/material.dart';
import 'package:nav_app/constants.dart';

class RegisterSucessPage extends StatelessWidget {
  const RegisterSucessPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          "You are successfully registered.",
          style: TextStyle(
            fontSize: 20,
          ),
        ),
        Padding(padding: EdgeInsets.all(4)),
        Text(
          "WOW WOW WOW ~",
          style: TextStyle(
            fontSize: 38,
            fontWeight: FontWeight.bold,
            color: Color(0xFFEF2828),
          ),
        ),
        SizedBox(height: defaultPadding * 2),
        Row(
          children: [
            Spacer(),
            Expanded(
              flex: 8,
              child: Image.asset(
                "assets/icons/registersuccess.gif",
                height: 400,
                width: 400,
              ),
            ),
            Spacer(),
          ],
        ),
        SizedBox(height: defaultPadding * 2),
      ],
    );
  }
}
