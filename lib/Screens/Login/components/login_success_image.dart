import 'package:flutter/material.dart';
import 'package:nav_app/constants.dart';

class LoginSucessPage extends StatelessWidget {
  const LoginSucessPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          "You have logged in.",
          style: TextStyle(
            fontSize: 20,
          ),
        ),
        Padding(padding: EdgeInsets.all(4)),
        Text(
          "Congratulations!",
          style: TextStyle(
            fontSize: 38,
            fontWeight: FontWeight.bold,
            color: Colors.green,
          ),
        ),
        SizedBox(height: defaultPadding * 2),
        Row(
          children: [
            Spacer(),
            Expanded(
              flex: 8,
              child: Image.asset(
                "assets/icons/loginsuccess.gif",
                height: 400,
                width: 400,
              ),
            ),
            Spacer(),
          ],
        ),
        SizedBox(height: defaultPadding * 2),
      ],
    );
  }
}
